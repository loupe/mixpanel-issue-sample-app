package com.loupeart.mixpaneltest

import android.content.Context
import com.mixpanel.android.mpmetrics.MixpanelAPI

/**
 * Created by eman8519 on 4/26/18.
 */
class MixpanelTracker private constructor(context: Context) {
    enum class AnalyticEvent(val eventName: String) {
        TEST_EVENT("Randy Fake Test Event"),
        TEST_EVENT_END("Randy Fake Test Event End")
    }

    private var mixpanel = MixpanelAPI.getInstance(context, BuildConfig.MIXPANEL_TOKEN)

    init {
        mixpanel.identify(mixpanel.distinctId)
        mixpanel.people.identify(mixpanel.distinctId)
    }

    companion object : SingletonHolder<MixpanelTracker, Context>(::MixpanelTracker)

    fun trackFakeEvent() {
        //Track the app opened, and then set a timer to wait for App Close.
        mixpanel.track(AnalyticEvent.TEST_EVENT.eventName)
        mixpanel.timeEvent(AnalyticEvent.TEST_EVENT_END.eventName)
    }

    fun trackFakeEventEnd() {
        mixpanel.track(AnalyticEvent.TEST_EVENT_END.eventName)
    }

    fun trackScreenView(name: String, properties: MutableMap<String, Any>?) {
        mixpanel.trackMap("$name Screen Opened", properties)
        mixpanel.timeEvent("$name Screen Closed")
    }

    fun trackScreenClosed(name: String, properties: MutableMap<String, Any>?) {
        mixpanel.trackMap("$name Screen Closed", properties)
    }

    fun trackEvent(event: AnalyticEvent, properties: MutableMap<String, Any>?) {
        mixpanel.trackMap(event.eventName, properties)
    }

    fun flush() {
        mixpanel.flush()
    }
}