/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.loupeart.mixpaneltest

import android.app.Activity
import android.os.Bundle

/**
 * Loads [MainFragment].
 */
class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MixpanelTracker.getInstance(this).trackFakeEvent()
    }

    override fun onResume() {
        super.onResume()
        MixpanelTracker.getInstance(this).trackScreenView("RandyFakeTest", null)
    }

    override fun onStop() {
        super.onStop()
        MixpanelTracker.getInstance(this).trackScreenClosed("RandyFakeTest", null)
    }

    override fun onDestroy() {
        MixpanelTracker.getInstance(this).trackFakeEvent()
        MixpanelTracker.getInstance(this).flush()
        super.onDestroy()
    }
}
